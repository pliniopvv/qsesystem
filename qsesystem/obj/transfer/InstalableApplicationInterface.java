/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qsesystem.obj.transfer;

/**
 *
 * @author PLINIO
 */
public interface InstalableApplicationInterface {

    public void install();

    public void start();

    public void close();
}
