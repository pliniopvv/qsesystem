/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qsesystem;

import qsesystem.obj.sys.SysView;

/**
 *
 * @author PLINIO
 */
public class QSESystem {

    public static void main(String[] args) {
        QSESystem.setLookAndFeel();
        SysView sv = new SysView();
        sv.start();
    }

    private static void setLookAndFeel() {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SysView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SysView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SysView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SysView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
}
